:mod:`infomedia.extension` --- Extensions
=========================================

.. automodule:: infomedia.extension


    .. autoclass:: PluginMount


        .. automethod:: get_plugin_classes()
        .. automethod:: get_plugin_names()
        .. automethod:: get_plugin()


    .. autoclass:: ActionProvider

    .. autoclass:: CommandProvider

        .. automethod:: get_subcommand()
        .. automethod:: add_subcommand()
        .. automethod:: __call__()

    .. autoclass:: SubCommand

    .. autoclass:: SubCommandProvider

