.. infomedia documentation master file, created by
   sphinx-quickstart on Sun Jun  8 21:15:45 2014.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

=========================
Infomedia Support Classes
=========================

Contents:

.. toctree::
   :maxdepth: 2

   infomedia_options.rst
   infomedia_errors.rst
   infomedia_extension.rst
   infomedia_vdict.rst
   infomedia_hash2cfg.rst
   infomedia_hash2cfg_defines.rst

..   infomedia_accounting.rst

..   
..   infomedia_patterns.rst
..   infomedia_template.rst

Indices and tables
==================

* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`

