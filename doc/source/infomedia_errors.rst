:mod:`infomedia.errors` --- Error Class
==========================================

.. automodule:: infomedia.errors


    .. autoclass:: Error()

        .. automethod:: add(error : str, options : Options object, level : str, where : str, tb : trackback object)
        .. automethod:: save(ext : str, tb : trackback object, options : Options object )
        .. automethod:: view(options : Options object)

        .. autoattribute:: inerror

