:mod:`infomedia.vdict` --- Caseless dictionary
==============================================


.. automodule:: infomedia.vdict
    :members:
