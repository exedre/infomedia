#!/usr/bin/python
# -*- coding: utf-8 -*-

##
##  This file is part of infomedia framework library
##
##  Copyright (c) 2011-2014 Infomedia Foundation
##
##  Author: Emmanuele Somma (emmanuele_DOT_somma_AT_infomedia_DOT_it)
##
##  Any parts of this program derived from this project,
##  or contributed by third-party developers are copyrighted by their
##  respective authors.
##
##  This program is free software; you can redistribute it and/or modify
##  it under the terms of the GNU General Public License as published by
##  the Free Software Foundation; either version 3 of the License, or
##  (at your option) any later version.
##
##  This program is distributed in the hope that it will be useful,
##  but WITHOUT ANY WARRANTY; without even the implied warranty of
##  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
##  GNU General Public License for more details.
##
##  You should have received a copy of the GNU General Public License
##  along with this program; if not, write to the Free Software
##  Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301, USA
##
##

"""
Generalized caseless dictionary with key expansion and localization

"""

import ast
import re

from itertools import compress, ifilter, imap

import logging
LOGGER = logging.getLogger(__name__)


# This is redefined here to avoid circular import (see defines.py)

def _variable_expansion(a_string, define_set):
    if define_set is None:
        return a_string
    if '$' in a_string:
        if hasattr(define_set, 'expand'):
            a_string = define_set.expand(a_string)
    return a_string


def _arithmetic_expansion(a_string):
    prev_str = None
    while '((' in a_string:
        a_string = re.sub(r'\(\(([-+*/0-9. ]+)\)\)', lambda m: \
                          unicode(eval(m.group(1))), a_string)

        # # Exit if last expansion has no effect

        if a_string == prev_str:
            break
        prev_str = ast.literal_eval(a_string)
    return a_string


def expansion(a_string, define_set):
    """expand a_string from the define_set dictionary

    Two kind of expansions are managed:

    - Variable Expansion for dollar sign variables
        $VAR
    - Arithmetic Expansion for double paren expressions
        (($VAR-1))

    :param a_string: string to expand
    :type a_string: str
    :param define_set: DefineSet
    :type define_set: DefineSet object

    :rtype: str

    """

    # Variable Expansion

    a_string = _variable_expansion(a_string, define_set)

    # Arithmetic Expansion

    a_string = _arithmetic_expansion(a_string)

    return a_string


###########################################

def get_list(collection, sep=','):
    """
    Split the collection string in a list by separator sep

    :param collection: string containing one or more element
    :type collection: unicode

    :param sep: separator character (defaults to ',')
    :type sep: unicode

    :returns: list of unicode
    """

    if collection:
        return [el.strip() for el in collection.split(sep)]


def boolean(instr):
    """return True/False from string specification:
    yes,true,on,1  -> True
    no,false,off,0 -> False

    :param instr: string, bool, int
    :type instr: string

    :returns: boolean
    """

    if instr is None:
        return False
    if isinstance(instr, (bool, int)):
        return bool(instr)
    if isinstance(instr, basestring):
        instr = instr.strip()
        if re.match(r'^(yes|true|on)$', instr, re.I):
            return True
        if re.match(r'^(no|false|off)$', instr, re.I):
            return False
        try:
            instr = eval(instr)
        except (NameError, SyntaxError):
            return False
    if isinstance(instr, (bool, int)):
        return bool(instr)
    LOGGER.debug('Unkown boolean %s',instr)
    return False


# def get_struct(conf,taskname):
#     if taskname is None:
#         return

#     section = taskname.upper().replace('_','-')
#     if conf.has_section(section):
#       tstruct = {}
#       tstruct['label']=taskname
#       for s in conf.options(section):
#         tstruct[s]=get_conf_val(conf,section,s)
#       return tstruct

#     LOGGER.warning("No section %s in configuration" % section )
#     return None

# def diff_dictionary(a,b):
#     """
#     riporta le differenze tra due dictionary
#     """
#     if a is None:
#         return (b,True)
#     if b is None:
#         return (a,False)
#     c = {}
#     d = False
#     for kk1,v1 in a.items():
#         k = kk1.encode('utf8','replace')
#         _val = v1.encode('utf8','replace')
#         if not b.has_key(k):
#             c[k]=(None,_val)
#             continue

#         if not b[k]==_val:
#             c[k]=(_val,b[k])
#             if k == "last updated": d = True

#     return (c,d)

def get_intlist(collection, sep=',', limit=None):
    """
    Split the collection string in a list of ints by separator sep

    :param collection: string containing one or more element
    :type collection: unicode

    :param sep: separator character (defaults to ',')
    :type sep: unicode

    :param limit: get only first C{limit} elements
    :type limit: int

    :returns: list of int elements
    """

    if collection:
        return get_numlist(collection, sep, limit, int)


def get_numlist(collection, sep=',', limit=None, klass=int):
    """
    Split the collection string in a list of ints by separator sep

    :param collection: string containing one or more element
    :type collection: unicode

    :param sep: separator character (defaults to ',')
    :type sep: unicode

    :param limit: get only first C{limit} elements
    :type limit: int

    :param klass: cast class for elements (defaults to int)
    :type klass: type or class

    :returns: list of C{klass} elements
    """

    if collection:
        if limit:
            return [klass(el.strip()) for (i, el) in
                    enumerate(collection.split(sep)) if i < limit]
        return [klass(el.strip()) for el in collection.split(sep)]


def needed(dataset, *needed_vars, **kw):
    """Check variables in dictionary"""

    error = 'E:GENERIC:001'
    exception = KeyError
    errmsg = '%s'

    if 'error' in kw:
        error = kw['error']
    if 'errmsg' in kw:
        errmsg = kw['errmsg']
    if 'exception' in kw:
        exception = kw['exception']
    _val = {}
    for k in needed_vars:
        if k:
            _val[k] = k not in dataset
    _wx = [k for (k, p) in _val.items() if p]
    if len(_wx) > 0:
        _str = ('%s -- ' + errmsg) % (error, ','.join(_wx))
        raise exception(_str)
            
    return len(_wx) == 0


def accepted(dataset, *accepted_vars):
    verb = dict()
    for k in sorted(dataset.keys()):
        if k:
            verb[k] = k in accepted_vars
    _wx = [k for (k, _val) in verb.items() if not _val]
    if len(_wx) > 0:
        if len(_wx) == 1:
            _str = 'Variable %s not accepted' % ','.join(_wx)
        elif len(_wx) > 1:
            _str = 'Variables %s not accepted' % ','.join(_wx)
        raise ValueError(_str)
    return len(_wx) == 0


def keycond(key):
    """select condition into a key"""

    (name, cond) = (key, None)
    if '|' in name:
        (name, cond) = name.split('|', 1)
    return (name, cond)


_COND_KEY = '__%s_COND'


class vdict(dict):

    """
    Caseless Dictionary

    """

    def _redefine_conditions(self):
        """Hash key and Transforms into condition if needed"""

        _hk = {}
        for (k, value) in self.items():
            (name, cond) = keycond(k)

            # Also delete keys to make them u/lcase

            del self[k]
            if cond is not None:
                LOGGER.debug('Key %s has condition %s', name, cond)
                self.set_with_cond(name, value, cond)
            else:
                _hk[self._key(name)] = value
        dict.update(self, _hk)

    def __init__(self, *args, **kwargs):
        self._defines = None
        dict.__init__(self, *args, **kwargs)

        # Redefine dict on conditions and key hash

        self._redefine_conditions()

    # # Conditioned values

    def get_cond(self, name):
        """get conditions for attribute, empty list if any"""

        cond = self.conditions(name)
        xcond = [x for x in compress(self[name], cond)]
        if xcond:
            return cond

    def conditions(self, name):
        """get condition list, return empty list if no conditions

        it also make condition expansion using defines
        """

        _cname = _COND_KEY % name
        _cond = list()
        if _cname in self:
            _cond = self[_cname]
            if self._defines:
                _cond = [expansion(x, self._defines) for x in _cond]
                #if hasattr(self._defines, 'expand'):
                #    _cond = [self._defines.expand(x) for x in _cond]
            _cond = [boolean(x) for x in  _cond]
        return _cond

    def get_condition_index(self, name):
        """get index of *first* verified condition if any, None otherwise
        (not have condition or not passed)"""

        cond = self.get_conditions_index(name)
        if cond:
            return cond[0]

    def get_conditions_index(self, name):
        """get all indexes of verified conditions or None otherwise"""

        cond = self.conditions(name)
        xcond = [x for x in compress(range(len(cond)), cond)]
        return xcond

    def get_with_or_without_cond(self, name):
        _val = self[name]
        if self.has_cond(name):
            _val = self.get_with_cond(name)
        return _val

    def cond_free_or_true(self, name):
        _cname = _COND_KEY % name
        if dict.has_key(self, _cname):
            return True
        if self.get_conditions_index(name):
            return True
        return False

    def cond_true(self, name):
        _cname = _COND_KEY % name
        if _cname in self and self.get_conditions_index(name):
            return True
        return False

    def get_with_cond(self, name):
        """get first value whose condition is true"""

        LOGGER.debug('GET CONDITION ON %s', name)
        try:
            _cname = _COND_KEY % name
            if _cname in self:
                assert len(self[_cname]) == len(self[name])
                i = self.get_condition_index(name)
                if i is not None:
                    return self[name][i]
        except SyntaxError:
            pass
        return None

    def has_cond(self, name):
        """return true if name has conditions"""

        _cname = _COND_KEY % name
        return dict.has_key(self, _cname)

    def set_with_cond(self, name, value, cond):

        LOGGER.debug('SETTING %s=%s in condition %s', name, value, cond)
        if value and cond:
            _cname = _COND_KEY % name
            if _cname not in self:
                (self[_cname], self[name]) = ([], [])
            try:
                i = self[_cname].index(cond)
                self[name][i] = value
            except ValueError:
                self[_cname].append(cond)
                self[name].append(value)
        else:
            dict.__setitem__(self, name, value)
        return value

    def _set_condname(self, name, value):
        (name, cond) = keycond(name)
        return self.set_with_cond(name, value, cond)

    def del_cond(self, name):
        if self.has_cond(name):
            _cname = _COND_KEY % name
            cond = self.conditions(name)
            del self[_cname]
            return cond

    def _key(self, key):
        return key

    def __getitem__(self, name):
        if name:
            return dict.__getitem__(self, self._key(name))

    def __setitem__(self, name, value):
        if '|' in name:
            cond = self._set_condname(self._key(name), value)
        else:
            cond = dict.__setitem__(self, self._key(name), value)
        return cond

    def __contains__(self, name):
        if self.has_cond(name):
            if self.get_cond(name):
                return dict.__contains__(self, self._key(name))
            return None
        else:
            return dict.__contains__(self, self._key(name))


    def has_key(self, name, defines=None):
        print self
        if defines:
            old_defines = self._defines
            self._defines = defines
        if self.has_cond(name):
            _ret = self.cond_true(name)
        else:
            _ret = dict.__contains__(self, self._key(name))
        if defines:
            self._defines = old_defines
        return _ret

    def has(self, name, *values, **kw):
        """check if one of the given values is in dict at key name

        uses case insensitive match if no `flags`
        you can `negate` list

        :param name: key name
        :type name: string
        """

        if not values or len(values) == 0:
            return None
        flags = re.I
        negate = False
        if 'flags' in kw:
            flags = kw['flags']
        if 'negate' in kw:
            negate = kw['negate']
        if name in self:
            _nm = self[name]
            for _val in values:
                if re.match(_val, _nm, flags) and not negate:
                    return _val

    # # Getter

    def xget(self, key, default=None, negate=False, delete=False,
             create=False, defines=None, section=None):
        """Get a value from the vdict o a sub dict

        :param key: label to get
        :type key: string

        :param default: default value if key not exists

        :param negate: return default if key not found
        :param delete: delete key if exists
        :param create: create key if not exists
        :param defines: expand key from define set
        :param section: take key from sub section
        """

        key = self._key(key)

        # setup defines
        old_defines = self._defines

        if defines:
            self._defines = defines

        # if [ in key then key is like [SECTION]]KEY
        #

        if '[' in key:
            _mat = re.match(r'^\[([^]]+)\](.+)$', key, re.I)
            if _mat:

                # assert groups are 2

                (section, key) = _mat.groups()

        if section:
            self._defines = old_defines
            if section not in self:
                return None
            return vdict(self[section]).xget(
                self._key(key), default, negate, delete, create, defines, )

        #
        # ##

        _key = grep(key, self, partial=False, limit=1)

        if _key and not negate:
            _key = _key[0]
            if _key in self:
                _val = self.get_with_or_without_cond(_key)

                if delete:
                    del self[_key]

                self._defines = old_defines
                if isinstance(_val, basestring):
                    return _val.strip()
                else:
                    return _val

            self._defines = old_defines

            if create:                
                LOGGER.error('Trying to create an existent value')
                raise ValueError(_key)

            return default
        else:
            if create:
                _ckey = self._key(key)
                self[_ckey] = default
            self._defines = old_defines
            return default

    def get_bool(self, key, default=False):
        _value = self.xget(self._key(key))
        _value = boolean(_value)
        if _value is not None:
            return _value
        return default

    def xget_bool(self, key, default=False):
        return self.get_bool(key, default)

    def xget_dictlist(self, key, default=None, negate=False, delete=False,
                      create=False, defines=None, section=None, sep=',', ):

        dictnames = self.xget_list(
            key, default, negate, delete, create, defines, section, )
        return self.select(*dictnames)

    def xget_list(self, key, default=None, negate=False, delete=False,
                  create=False, defines=None, section=None, sep=',', ):

        _rvs = self.xget(key, default, negate, delete, create, defines, section)
        if _rvs:
            return get_list(_rvs, sep=sep)
        return []

    def xget_intlist(self, key, default=None, negate=False, delete=False,
                     create=False, defines=None, section=None, sep=',',
                     limit=None):

        _val = self.xget(key, default, negate, delete, create, defines, section)
        if _val:
            return get_intlist(_val, sep=sep)

    def xget_dict(self, key, default=None, negate=False, delete=False,
                  create=False, defines=None, section=None):

        _val = self.xget(key, default, negate, delete, create, defines, section)
        if _val:
            return self.__class__(_val)

    def xget_int(self, key, default=None, negate=False, delete=False,
                 create=False, defines=None, section=None):

        _val = self.xget(key, default, negate, delete, create, defines, section)
        if _val:
            return int(_val)

    def select(self, *regexps, **kw):
        """
        Select variables name with a regular expression

        :param regexps: a regular expression matching the name
        :type regexps: list of unicode
        :returns: udict or empty
        """

        _rxps = True
        if 'regexp' in kw:
            _rxps = kw['regexp']
        _res = udict()
        for _regexp in regexps:
            _model = _regexp
            if not _rxps:
                _model = '^' + _model + '$'
            _names = grep(_model, self)
            if _names:
                for _name in _names:
                    _res[_name] = self[_name]
        return _res

    def select_as_list(self, *regexps):
        """
        Select variables name with a regular expression

        :param regexps: a regular expression matching the name
        :type regexps: list of unicode
        :returns: list or empty
        """

        _res = list()
        for _regexp in regexps:
            _names = grep(_regexp, self)
            if _names:
                _res.extend([self[name] for name in _names])
        return _res

    def select_nulls(self, *regexps):
        """
        Select variables name with a regular expression
        Returns a dict of given elements each with None value

        :param regexps: a regular expression matching the name
        :type regexps: list of unicode
        :returns: udict or None
        """

        _res = udict()
        for _regexp in regexps:
            _names = grep(_regexp, self)
            if _names:
                for _name in _names:
                    _res[_name] = None
        return _res

    def remove(self, *regexps):
        """
        Remove keys with regexps

        :param regexps: list of regexp matching keys
        """

        for _regexp in regexps:
            _names = grep(_regexp, self)
            for _name in _names:
                self.purge(_name)
        return self

    def purge(self, key):
        if key in self:
            del self[key]
            if self.has_cond(key):
                self.del_cond(key)


class udict(vdict):

    def __init__(self, *args, **kwargs):
        vdict.__init__(self, *args, **kwargs)

    def _key(self, key):
        return key.upper()

    def update(self, indict):
        _hdict = udict(indict)
        vdict.update(self, _hdict)

class ldict(vdict):

    def __init__(self, *args, **kwargs):
        vdict.__init__(self, *args, **kwargs)

    def _key(self, key):
        return key.lower()

    def update(self, indict):
        _hdict = ldict(indict)
        vdict.update(self, _hdict)


def fmt_dict(a_dict, cond=lambda x, y: True):
    _ret = []
    for (k, _val) in a_dict.items():
        if cond(k, _val):
            _ret.append('%s=%s' % (str(k), str(_val)))
    return ', '.join(_ret)


def dictview(a_dict, sep=','):

    if a_dict is None:
        return u'None'

    if len(a_dict) == 0:
        return u'[len=(0)], Empty'

    # this a_dict is not a dict

    if isinstance(a_dict, basestring):
        return a_dict

    if isinstance(a_dict, (list, tuple)):
        _str = u'[len=(%d)|' % len(a_dict)
        _str += sep.join([dictview(_val) for _val in a_dict])
        _str += u']'
        return unicode(_str)

    if not isinstance(a_dict, (dict)):
        return u'Error'

    # this a_dict is a dict for sure

    _len = [len(k) for k in a_dict.keys()]
    lrg = max(_len)

    _str = '{'
    for (k, _vk) in sorted(a_dict.items()):
        if isinstance(_vk, dict):
            if len(_vk) == 0:
                _str += ' Empty'
                continue
            _str += '%s = {  \n' % k.rjust(lrg)
            lrg1 = max([len(kk1) for kk1 in _vk.keys()])
            for (i, j) in sorted(_vk.items()):
                _str += ' %s %s = %s | %s \n' % (' '.rjust(lrg), i.rjust(lrg1),
                                                 j,
                                                 type(j))
            _str += ' %s }\n' % ' '.rjust(lrg)
        else:
            _str += '%s = %s | %s\n' % (k.rjust(lrg), _vk, type(_vk))
    _str += '}'
    return _str


def logdict(func, info, adict):
    func('%s -- %s %s', info, type(adict), dictview(adict))


def grepm(a_list, *strings, **kw):
    """Grep strings, returns matchobjs"""
    _ret = list()
    for _str in strings:
        _mat = mgrep(_str, *a_list, **kw)
        if _mat:
            _ret.extend(_mat)
    return _ret


def mgrep(string, *args, **kw):
    if 'flags' in kw:
        flags = kw['flags']
        return grep(string, args, *flags)
    return grep(string, args)


def grep(string, a_list, *flags, **kw):
    """ grep regular expression from the list or dict keys, returning list

    :param partial: is False if should search full key (^key$)

    :param limit: returns only limit results
    """

    limit = None
    if 'partial' in kw and not kw['partial']:
        string = '^' + string + '$'
    if 'limit' in kw:
        limit = kw['limit']
    expr = re.compile(string, *flags)
    the_names = []
    if isinstance(a_list, (list, tuple)):
        the_names = a_list
    elif isinstance(a_list, (dict, udict, ldict)):
        the_names = sorted(a_list.keys())
    else:
        LOGGER.debug('Collection mismatch in grep (type is %s)', type(a_list))
        return []
    _ret = [ k for k in  ifilter(expr.search, the_names )]
    if limit and _ret:
        return _ret[:limit]
    return _ret


def select(a_dict, *regexps):
    """
    Select variables name with a regular expression

    :param regexps: a regular expression matching the name
    :type regexps: list of unicode
    :returns: udict or None
    """

    _res = udict()
    for _regexp in regexps:
        _names = grep(_regexp, a_dict)
        for _name in _names:
            _res[_name] = a_dict[_name]
    return _res


def compose_dicts(dicts, accept=None, klass=udict):
    """Each arg is a dict. Returns composition of dicts in reverse order"""

    
    _dict = klass(dicts[0])
    for _ax in dicts[1:]:
        if accept and len(accept) > 0:
            _cd = select(_ax, *accept)
        else:
            _cd = _ax
        _dict.update(klass(_cd))
    _dict = extend_booleans(_dict)
    return _dict


def extend_booleans(_dict):
    klass = _dict.__class__
    _ret = klass()

    for (key, value) in _dict.items():
        if isinstance(value, basestring):

            if re.match('^yes|on|true|1$', value, re.I):
                _val = klass({'ACTIVE': True, key: value})
                _ret.update(klass({key: _val}))
                continue

            if re.match('^no|off|false|1$', value, re.I):
                _val = klass({'ACTIVE': False, key: value})
                _ret.update(klass({key: _val}))
                continue

        _ret.update(klass({key: value}))

    return _ret


def save_dict(fname, contents, debug=True, ext=None):

    import codecs
    import os
    from os.path import exists
    _name = fname
    if ext is not None:
        _name = fname + '.' + ext
    if exists(_name):
        os.unlink(_name)
    if debug:
        codecs.open(_name, 'w', 'utf-8').write(dictview(contents))




__all__ = """
expansion
boolean
get_list
get_intlist
get_numlist
needed
accepted
keycond
fmt_dict
dictview
logdict
grepm
mgrep
grep
select
compose_dicts
extend_booleans
save_dict
vdict
udict
ldict
""".split()
