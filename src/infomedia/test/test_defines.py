# Copyright 2011-2012 by Infomedia Foundation

# @OPENSOURCE_HEADER_START@
# @OPENSOURCE_HEADER_END@

from datetime import datetime

import unittest

import infomedia
from infomedia.hash2cfg.defines import *

class MacroExpansionTest(unittest.TestCase):

    def setUp(self):
        self._date = datetime.now()

    def test_defines(self):
        "DEFAULT_DEFINES['THISMONTH']"
        self.assertEqual(str(self._date.month),
                         DEFAULT_DEFINES['THISMONTH'])

    def test_expandfuncs(self):
        self.assertRaises(AssertionError,expandfuncs,[1,])
        self.assertRaises(AssertionError,expandfuncs,'')
        self.assertEqual(expandfuncs('FOOBAR'),'FOOBAR')
        self.assertEqual(expandfuncs('%MONTH(1)'),u'#_(Jan)#1#c#')
        self.assertEqual(expandfuncs('%MONTH(12)'),u'#_(Dec)#1#c#')
        self.assertEqual(expandfuncs('%MONTH(13)'),u'#_(Jan)#1#c#')
        #import pdb; pdb.set_trace()
        #self.assertEqual(expandfuncs('%FYEAR(2000,2,2)'),u'#_(Jan)#1#c#')
        pass

    def test_expandlistfunc(self):
        pass

    def test_fullexpand(self):
        pass


class DefineSetTest(unittest.TestCase):

    def test_00constructor_empty(self):
        pass

    def test_01constructor_indict(self):
        pass

    def test_02update(self):
        pass

    def test_02update_from_string(self):
        pass

    def test_03expand(self):
        pass



                  
