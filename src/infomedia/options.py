#!/usr/bin/python
# -*- coding: utf-8 -*-

##
##  This file is part of infomedia framework library
##
##  Copyright (c) 2011-2012 Infomedia Foundation
##
##  Author: Emmanuele Somma (emmanuele_DOT_somma_AT_infomedia_DOT_it)
##
##  Any parts of this program derived from this project,
##  or contributed by third-party developers are copyrighted by their
##  respective authors.
##
##  This program is free software; you can redistribute it and/or modify
##  it under the terms of the GNU General Public License as published by
##  the Free Software Foundation; either version 3 of the License, or
##  (at your option) any later version.
##
##  This program is distributed in the hope that it will be useful,
##  but WITHOUT ANY WARRANTY; without even the implied warranty of
##  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
##  GNU General Public License for more details.
##
##  You should have received a copy of the GNU General Public License
##  along with this program; if not, write to the Free Software
##  Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301, USA
##
##


from infomedia.vdict import grep, udict

class Options(object):
    """
    The Options class is a dictionary where you can access slots which dot notation::

        options = Options({'foo':'bar')
        print options.foo
    
    A way to define new options is the `setattr` function, as in::

        setattr(option,'foobar','baz')
        print options.foobar

    """

    _we_are_one = {}

    @classmethod
    def setup(cls,**kwargs):
        cls._we_are_one.update(kwargs)

    def __init__(self, **kwargs):
        self._we_are_one.update(kwargs)
        self.__dict__ = self._we_are_one

    def get(self,label):
        """get an options

        :param label: the name of the option
        :type label: str
        :rtype: the value of the option
        """
        if label in self.__dict__:
            return self.__dict__[label]

    def update(self, a_dict):
        """
        update options from a dictionary

        :param dict a_dict: input dictionary
        """
        assert isinstance(a_dict,(dict))

        for (k, j) in a_dict.items():
            setattr(options, k, j)

    def select(self, *regexps, **kw):
        """select variables name with a regular expression

        :param regexps: a regular expression matching the name
        :type regexps: list of str
        :rtype: udict or None
        """

        _is_regexp = True
        if kw.has_key('regexp'):
            _is_regexp = kw['regexp']
        _res = udict()
        for _regexp in regexps:
            _model = _regexp
            if not _is_regexp:
                _model = '^' + _model + '$'
            _names = grep(_model, self.__dict__.keys())
            if _names:
                for _name in _names:
                    _res[_name] = self.__dict__[_name]

        return _res


__all__ = """
 Options
""".split()
