
##
##  This file is part of e4t
##
##  Copyright (c) 2011-2012 Banca d'Italia - Area Ricerca Economica e
##                          Relazioni Internazionali
##
##  Author: Emmanuele Somma (emmanuele_DOT_somma_AT_bancaditalia_DOT_it)
##          Area Ricerca Economica e Relazioni Internazionali
##          Banca d'Italia
##
##  Any parts of this program derived from this project,
##  or contributed by third-party developers are copyrighted by their
##  respective authors.
##
##  This program is free software; you can redistribute it and/or modify
##  it under the terms of the GNU General Public License as published by
##  the Free Software Foundation; either version 3 of the License, or
##  (at your option) any later version.
##
##  This program is distributed in the hope that it will be useful,
##  but WITHOUT ANY WARRANTY; without even the implied warranty of
##  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
##  GNU General Public License for more details.
##
##  You should have received a copy of the GNU General Public License
##  along with this program; if not, write to the Free Software
##  Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301, USA
##
##

import csv
import json
import re
from datetime import datetime

from infomedia.vdict import udict

import logging
logger = logging.getLogger(__name__)

ds_url  = re.compile('^([^~]+)((?:~[^~]+)+)$')
# START
ds_qual = (
    (re.compile('([0-9]{4}-[0-1][0-9]-[0-3][0-9])',re.I) , 'START', lambda x: ('DATE', datetime.strptime(x[0],'%Y-%m-%d')) ),
    (re.compile('-([0-9]+)(Y|D)',re.I) , 'START' , lambda x: (x[2].upper(),x[1])),
    (re.compile( ':([0-9]{4}-[0-1][0-9]-[0-3][0-9])',re.I) , 'END' , lambda x: ('DATE', datetime.strptime(x[0],'%Y-%m-%d')) ),
    (re.compile(':-([0-9]+)(Y|D)',re.I) , 'START'  , lambda x: (x[2].upper(),x[1])),
    (re.compile('=([^~]+)',re.I), 'DATATYPE' ),
    (re.compile('@([0-9-]+)',re.I), 'POINT' ),
    (re.compile('(ohlcv|perf|perfli|xref|list)',re.I), 'TEMPLATE' ),
    (re.compile('(rep)',re.I),'REPORT'),
    (re.compile('na=([^~]+)',re.I),'MISSING'),
    (re.compile('(d|w|m|y)',re.I),'FREQ'),
    )

ds = None

def understand(v):
    (start,end,url) = (v[1]['START'],v[1]['END'],v[0][0]['Instrument'])
    logger.debug('Cerco i limiti di %s',url)
    q = []
    _func = []
    m = ds_url.match(url)
    if m:
        _r = m.groups()
        logger.debug('Riconosciuta %s',_r)
        _func = [ _r[0], {} ]
        _quals = []
        if len(_r)>0:
            _quals = _r[1].split('~')
            for _q in _quals:
                if not _q:
                    continue
                print "QUAL=",_q
                for Q in ds_qual:
                    n = Q[0].match(_q)
                    if n:
                        _r=Q[2](n.groups()) if len(Q)>2 and Q[2] else (n.groups()[0],True)
                        # pprint(_r)
                        _func[1][Q[1]]=dict((_r[0],_r[1:]))
    return _func

def dstream_lim(lim,v):
    print lim,"=",
    global ds
    if not ds:
        ds=understand(v)
    try:
        return "ultimi %s anni" % ds[1][lim]['Y']
    except:
        try:
            return "ultimi %s giorni" % ds[1][lim]['D']
        except:
            print ".....",ds[1]
            print ".....",ds[1][lim]
            print ".....",ds[1][lim]['DATE']
            try:
                return "%s" % ds[1][lim]['DATE'].strftime("%d/%m/%Y")
            except:
                return None

_repr_providers = {
    'BASE' : {
        'req':  lambda x: x,
        'name': lambda x: x[1]['NAME'] ,
        'proc': lambda x: x[1]['PROC'] ,
        'limits': lambda x: str(x[1]['START'])+"-"+str(x[1]['END']),
        },
    'DSTREAM' : {
        'req': lambda x: x[0][0]['Instrument'],
        'start': lambda x: dstream_lim('START',x),
        'end': lambda x: dstream_lim('END',x),
        }
        }
_repr_items = {
    'trac' :
        {
        'title':      lambda x: '= %s ='   % x ,
        'subtitle':   lambda x: '== %s ==' % x ,
        'item':       lambda x: ' * %s'    % x ,
        'td':         lambda x: '||%s'     % x,
        },
    'wiki' :
        {
        'title':      lambda x: '= %s ='   % x,
        'subtitle':   lambda x: '== %s ==' % x,
        'item':       lambda x: '* %s'     % x,
        'btable':      lambda x: '{|%s'   % x,
        'etable':      lambda x: '|}%s'   % x,
        'tr':         lambda x: '|%s\n|-'      % x,
        'td':         lambda x: '%s||'         % x,
        }
    }

def __init__(self):
    pass

def _get_p(who,what,x):
    try:
        return _repr_providers[who][what](x)
    except KeyError, exc:
        pass

    try:
        return _repr_providers['BASE'][what](x)
    except KeyError, exc:
        pass

    return x

def _get_i(who,what,x):
    try:
        return _repr_items[who][what](x)
    except KeyError, exc:
        return x

class checkpoint(udict):
    _we_are_one = {}

    def __init__(self):
        self.__dict__ = self._we_are_one

    def ok(self,name,phase):
        self.log(name,phase,'OK')
    def ko(self,name,phase):
        self.log(name,phase,'KO')
    def error(self,name,phase):
        self.log(name,phase,'EE')
    def WARN(self,name,phase):
        self.log(name,phase,'WR')

    def log(self,name,phase,msg):
        if not self.has_key(name):
            self[name]={}
            if not hasattr(self,'elems'):
                self.elems= []
            self.elems.append(name)

        if not self[name].has_key(phase):
            self[name][phase]=msg

    def report(self,job,*phases,**kw):
        if not hasattr(self,'elems'):
            self.elems= []
        small=False
        if kw.has_key('small'):
            small=kw['small']
        str = ""
        if not small:
            str += "Checkpoint Report\n"
            str += "%20s | " % ""
            for p in phases:
                str += "%4s " % p
            str += "\n"
        if job:
            elems = (job,)
        else:
            elems = self.elems
        for elx in elems:
            el = elx #[0]
            str += "%20s |" % el
            if self.has_key(el):
                for p in phases:
                    H = self[el]
                    V = '--'
                    if H.has_key(p):
                        V = H[p]
                    if V=="EE": V=V+"*"
                    str += "%4s " % V
            else:
                str += " MISSING"
            if not small:
                str += "\n"
        return str

chkpnt = checkpoint()

from enum import Enum
import getpass
import socket
import time
import uuid
import urlparse
from os.path import expanduser,expandvars,exists,join,basename,dirname

status = Enum('KO','WARN','OK')

class acctDevice(object):
    kind = None

    def __init__(self,url):
        self._url = url
        self._parsed = urlparse.urlparse(url)
        self._working = True

    def open(self):
        pass

    def close(self):
        pass

    def write(self,Section,Key=None,Value=None,Status=None):
        pass

class dbAcctDevice(acctDevice):
    pass

class dictAcctDevice(acctDevice):
    pass

class fileAcctDevice(acctDevice):


    kind = 'file'
    delimiter = ';'
    quotechar = '"'
    quoting = csv.QUOTE_MINIMAL

    def __init__(self,url):
        acctDevice.__init__(self,url)
        self._filename = expanduser(expandvars(self._parsed.path))
    def open(self):
        try:
            self._fh=open(self._filename,'a+')
            self._working = True
        except IOError, exc:
            logger.info('Cannot use accounting for file %s',self._filename)
        self._writer=csv.writer(self._fh,
                                delimiter=self.delimiter,
                                quotechar=self.quotechar,
                                quoting=self.quoting)
        #        self._writer.writeheader(["User",
        #                          "Host",
        #                          "UUID",
        #                          "Timestamp",
        #                          "Section",
        #                          "Key",
        #                          "Value",
        #                          "Status"])
    def close(self):
        if self._working:
            self._fh.close()

    def write(self,User,Host,UUID,Timestamp,Section,Key=None,Value=None,Status=None,**kw):
        if not self._working:
            return
        if Status is None: Status = status.OK
        jdump = json.dumps(kw)
        self.open()
        self._writer.writerow([User,Host,UUID,Timestamp,Section,Key,Value,Status,jdump])
        self.close()

class mysqlAcctDevice(dbAcctDevice):
    kind = 'mysql'

class redisAcctDevice(dictAcctDevice):
    kind = 'redis'

class csvAcctDevice(fileAcctDevice):
    kind = 'csv'


acct_devices = [fileAcctDevice,mysqlAcctDevice,redisAcctDevice,csvAcctDevice]

def getAcctDevice(_kind):
    for x in acct_devices:
        if x.kind == _kind:
            return x

def accounting_device(url):
    u = urlparse.urlparse(url)
    ad = getAcctDevice(u.scheme)
    return ad(url)

class Accounting(object):
    User  = getpass.getuser()
    Host  = socket.gethostname()
    UUID  = uuid.uuid1()
    _options = None
    _device  = None
    _working = False
    _messages = []
    __we_are_one = {}

    @classmethod
    def setup(cls,options):
        cls._options = options
        if hasattr(options,'switch_accounting'):
            cls._working = options.switch_accounting
        if not hasattr(options,'accounting_url'):
                cls._working = False
                return
        if options.accounting_url:
            cls._device  = accounting_device(options.accounting_url)
            if cls._device is None:
                logger.error('Cannot open accounting device %s',options.accounting_url)
                cls._working = False

    def __init__(self):
        self.__dict__ = self.__we_are_one

    @property
    def Timestamp(self):
        return time.time()

    def write(self,Section,Key=None,Value=None,Status=None,**kw):
        Status = Status if Status is not None else status.OK
        jdump = json.dumps(kw)
        self._messages.append([self.User,self.Host,self.UUID,self.Timestamp,
                               Section,Key,Value,Status,jdump])
        if self._working:
            self._device.write(self.User,self.Host,self.UUID,
                               self.Timestamp,Section,Key,Value,Status,**kw)

    def ok(self,Section,Key=None,Value=None,**kw):
        self.write(Section,Key,Value,status.OK,**kw)

    def ko(self,Section,Key=None,Value=None,**kw):
        self.write(Section,Key,Value,status.KO,**kw)

    def warn(self,Section,Key=None,Value=None,**kw):
        self.write(Section,Key,Value,status.WARN,**kw)

    def has_ko(self):
        ko = False
        for User,Host,UUID,Timestamp,Section,Key,Value,Status,jdump in self._messages:
            if Status == 'KO':
                ko = True
                break
        return ko

    def info(self):
        s = "\n"
        t = None
        t0 = 0
        for User,Host,UUID,Timestamp,Section,Key,Value,Status,jdump in self._messages:
            if t is None:
                t = Timestamp
                t0= Timestamp

            fmt = "%s %s %s %s %s %s"

            t1 = (Timestamp-t)
            t2 = (Timestamp-t0)
            t1 = ("%3.1f" % t1).rjust(5) if t1>0 else "---".rjust(5)
            t2 = ("%8.0f" % t2).rjust(9) if t2>0 else "---".rjust(9)
            s += fmt % (Status,
                        Section.rjust(5),
                        Key.ljust(8),
                        Value.ljust(20),
                        t1,t2)
            if Status == 'KO':
                jmsg = json.loads(jdump)
                s += jmsg
            s += "\n"
            t = Timestamp
        return s

_accounting = {}

