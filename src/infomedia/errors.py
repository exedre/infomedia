#!/usr/bin/python
# -*- coding: utf-8 -*-

##
##  This file is part of infomedia framework library
##
##  Copyright (c) 2011-2014 Infomedia Foundation
##
##  Author: Emmanuele Somma (emmanuele_DOT_somma_AT_infomedia_DOT_it)
##
##  Any parts of this program derived from this project,
##  or contributed by third-party developers are copyrighted by their
##  respective authors.
##
##  This program is free software; you can redistribute it and/or modify
##  it under the terms of the GNU General Public License as published by
##  the Free Software Foundation; either version 3 of the License, or
##  (at your option) any later version.
##
##  This program is distributed in the hope that it will be useful,
##  but WITHOUT ANY WARRANTY; without even the implied warranty of
##  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
##  GNU General Public License for more details.
##
##  You should have received a copy of the GNU General Public License
##  along with this program; if not, write to the Free Software
##  Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301, USA
##
##

import codecs

from collections import defaultdict

import traceback
import sys
from os.path import join

from infomedia.vdict import udict

class Error(object):
    """
    
    """
    _errors = udict()
    _n = 0

    def save(self, ext, tb, options):
        """
        Save an error file

        :param ext: file extension (not used yet)
        :type ext: 
        :param tb: trackback
        :type trackback object:
        :param options: Application Options
        :type Options class:
        

        :rtype:  ( filename, error message, stackobject )
        """
        _job = options.job
        _stack = u''.join(traceback.format_tb(tb[2]))
        _exception = u'%s' % tb[1]
        _texc = u'%s' % tb[0]
        _bmsg = u'%10s | %s' % (_job.upper(), _exception)
        _msg = _bmsg + '\n' + _stack + _exception + '\n' + _texc + '\n'
        _fname = '%s.%03d.exc' % (_job, self._n)
        _fullname =  join(options.output_path, _fname)
        _msg = _msg.encode('utf-8', 'ignore')
        codecs.open(_fullname, 'w').write(_msg)
        self._n += 1
        return (_fname, _bmsg, _stack)


    def add(self, message, options, level, where, tb):
        """
        Add the message to the list

        :param message: message to append to the list
        :type message: str
        :param options: Application Options
        :type Options class:
        :param level: level
        :type level: str
        :param where: where the error is
        :type where: str
        :param tb: trackback
        :type trackback object:

        """
        if not message:
            return
        label = options.get(level)
        if level not in self._errors:
            self._errors[level] = udict()
        if label not in self._errors[level]:
            self._errors[level][label] = defaultdict(list)
        self._errors[level][label][where].append((message, tb))


    def view(self, options):
        """
        Prints the errors list

        :param options: Application Options
        :type Options class:
        """
        for (level, l_dict) in self._errors.items():
            for (label, w_dict) in l_dict.items():
                for (errors, v) in w_dict.items():
                    for (exc, tb) in v:
                        (fname, msg, stack) = self.save(exc, tb,
                                options)
                        print '%s %s %s %s (see file %s)' \
                            % (level.rjust(10), label.ljust(10),
                               label.ljust(10), msg, fname)


    @property
    def inerror(self):
        """
        True if errors
        """
        return len(self._errors) > 0

    def _update(self, errors, options, level, where):
        if not errors:
            return
        LEVEL = level.upper()
        level = options.get(LEVEL)        
        if level not in self._errors:
            self._errors[level] = udict()
        if label not in self._errors[level]:
            self._errors[level][label] = defaultdict(list)
        self._errors[level][label][where].extend(errors)

    def _format(self, exc, options):
        job = options.job
        stack = u''.join(traceback.format_tb(sys.exc_info()[2]))
        stack = (unicode(stack) if options.switch_debug else u'')
        msg = u'%10s | %s | T:%s' % (job.upper(), sys.exc_info()[1],
                sys.exc_info()[0])
        codecs.open('%s.%03d.exc' % (job, self._n), 'w'
                    ).write(msg.encode('utf-8', 'ignore'))
        self._n += 1
        return (msg, stack)

__all__ = ['Error']
