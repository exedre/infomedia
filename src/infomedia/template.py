#! /usr/bin/env python
# -*- coding: utf-8 -*

##
##  This file is part of e4t 
##
##  Copyright (c) 2011-2012 Banca d'Italia - Area Ricerca Economica e
##                          Relazioni Internazionali
##
##  Author: Emmanuele Somma (emmanuele_DOT_somma_AT_bancaditalia_DOT_it)
##          Area Ricerca Economica e Relazioni Internazionali 
##          Banca d'Italia
##		  
##  Any parts of this program derived from this project,
##  or contributed by third-party developers are copyrighted by their
##  respective authors.
##
##  This program is free software; you can redistribute it and/or modify
##  it under the terms of the GNU General Public License as published by
##  the Free Software Foundation; either version 3 of the License, or
##  (at your option) any later version.
##
##  This program is distributed in the hope that it will be useful,
##  but WITHOUT ANY WARRANTY; without even the implied warranty of
##  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
##  GNU General Public License for more details.
##
##  You should have received a copy of the GNU General Public License
##  along with this program; if not, write to the Free Software
##  Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301, USA
##
##
import re
import sys
import e4t

from os.path import dirname, exists,basename,join
from pprint import pprint

from infomedia.vdict import udict
from e4t.utils import isofkind,_i

from infomedia.hash2cfg import cfg2hash,get_list,boolean,tuple_of_floats

import logging
logger = logging.getLogger(__name__)

_accounting={}

class Template(object):
    def __init__(self,fname):
        self._fname=fname

    def getTemplate(self,key):
        path = join(self.dir,'templates')
        if not exists(path):
            logger.error('path %s not exists',path)
            raise ValueError
        fname = join(path,"%s.template" % key)
        if not exists(fname):
            logger.error('template file %s not exists',fname)
            raise ValueError
        contents = open(fname,'r').read()
        return contents
        
        
        
