#!/usr/bin/python
# -*- coding: utf-8 -*-

##
##  This file is part of infomedia framework library
##
##  Copyright (c) 2011-2014 Infomedia Foundation
##
##  Author: Emmanuele Somma (emmanuele_DOT_somma_AT_infomedia_DOT_it)
##
##  Any parts of this program derived from this project,
##  or contributed by third-party developers are copyrighted by their
##  respective authors.
##
##  This program is free software; you can redistribute it and/or modify
##  it under the terms of the GNU General Public License as published by
##  the Free Software Foundation; either version 3 of the License, or
##  (at your option) any later version.
##
##  This program is distributed in the hope that it will be useful,
##  but WITHOUT ANY WARRANTY; without even the implied warranty of
##  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
##  GNU General Public License for more details.
##
##  You should have received a copy of the GNU General Public License
##  along with this program; if not, write to the Free Software
##  Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301, USA
##
##

import imp
import os
import sys

from os.path import join, splitext

# from e4t.utils import E4tSystemError

from infomedia.accounting import Accounting

from infomedia.options import Options
from infomedia.errors import Error
from infomedia.vdict import get_list

import logging
LOGGER = logging.getLogger(__name__)
ACCT = Accounting()

MODULE_EXTENSIONS = ('.py', '.pyc', '.pyo')


def m__import__(name, path=None, globals=None, locals=None, fromlist=None):
    """module importing"""

    # Fast path: see if the module has already been imported.

    if name in sys.modules:
        return sys.modules[name]

    # If any of the following calls raises an exception,
    # there's a problem we can't handle -- let the caller handle it.
    # logger.debug('Find Module %s on %s',name,path)

    (_fp, pathname, description) = imp.find_module(name, [path])
    try:

        return imp.load_module(name, _fp, pathname, description)

    finally:

        # Since we may exit via an exception, close fp explicitly.

        if _fp:
            _fp.close()


def package_contents(package_name, subdir=None,
                     extensions=MODULE_EXTENSIONS, split=True):

    (fname, pathname, _) = imp.find_module(package_name)
    if extensions:

        def endsw(module):
            return module.endswith(extensions)

    else:

        def endsw(module):
            return True

    if split:

        def splitw(module):
            return splitext(module)[0]

    else:

        def splitw(module):
            return module

    if fname:
        raise ImportError('Not a package: %r', package_name)

    # Use a set because some may be both source and compiled.

    if subdir:
        pathname = join(pathname, subdir)

    _set = list()
    for module in os.listdir(pathname):
        if endsw(module) and not module.endswith('~') \
            and not module.startswith('_') \
            and not module.startswith('.#'):
            _set.append(splitw(module))
    return _set


def extension_functions(package_name, subdir):
    (_, path, _) = imp.find_module(package_name)
    lfunctions_n = package_contents(package_name, subdir)
    lfunctions = dict()
    path = join(path, subdir)
    for name in lfunctions_n:
        func = None
        mod = m__import__(name, path)
        if hasattr(mod, name.upper()):
            func = getattr(mod, name.upper())
            lfunctions[name.upper()] = func()
    return lfunctions


def extension_object(package_name, subdir):
    (_, path, _) = imp.find_module(package_name)
    lopbjects_n = package_contents(package_name, subdir)
    lopbjects = dict()
    path = join(path, subdir)
    for name in lopbjects_n:
        func = None
        mod = m__import__('%s' % name, path)
        if hasattr(mod, name.upper()):
            func = getattr(mod, name.upper())
            lopbjects[name.upper()] = func
    return lopbjects


def object_set(package_name, subdir, opt_key, p_attr,
               options=Options()):
    """Get a dict of callable from a directory

    @param package_name:
    @param path:
    @param subdir: in the path
    @param dir_name:
    @param opt_key: for external extension
    @param p_attr: name of attribute with selection name
    """

    _object_set = extension_object(package_name, subdir)
    if opt_key is not None and hasattr(options, opt_key):
        (package_name, _, subdir, _) = get_list(getattr(options, opt_key))
        ps2 = extension_object(package_name, subdir)
        if ps2:
            _object_set.update(ps2)
    _object_dict = dict()
    for j in _object_set.values():
        if hasattr(j, p_attr) and isinstance(getattr(j, p_attr), basestring):
            _key = '^%s$' % getattr(j, p_attr)
            _object_dict[_key] = j
    return _object_dict


class PluginMount(type):

    def __init__(cls, name, bases, attrs):

        if not hasattr(cls, 'plugins'):

            # This branch only executes when processing the mount point itself.
            # So, since this is a new plugin type, not an implementation, this
            # class shouldn't be registered as a plugin. Instead, it sets up a
            # list where plugins can be registered later.

            cls.plugins = dict()
        else:

            # This must be a plugin implementation, which should be registered.
            # Simply appending it to the list is all that's needed to keep
            # track of it later.

            if bases:
                cls.plugins[name] = cls

    def get_plugins(cls, *args, **kwargs):
        return [p(*args, **kwargs) for p in cls.plugins.values()]

    def get_plugin_classes(cls, *args, **kwargs):
        return cls.plugins.items()

    def get_plugin_names(cls, *args, **kwargs):
        return cls.plugins.keys()

    def get_plugin(cls, key, *args, **kwargs):
        return cls.plugins[key.upper()]


class ActionProvider(object):

    __metaclass__ = PluginMount


class CommandProvider(object):

    __metaclass__ = PluginMount

    @classmethod
    def add_subcommand(cls, key, _cls):
        if not hasattr(cls, 'subcommands'):
            cls.subcommands = {}
        cls.subcommands[key] = _cls

    @classmethod
    def get_subcommand(cls, key):
        return cls.subcommands[key.upper()]

    def __call__(self, options, *args, **kwargs):
        error = Error()
        setattr(options, 'joblist', options.jobfile)

        execs = ['options']
        if 'exec' in options.options:
            execs = options.options['exec']

        for execf in execs:
            setattr(options, 'exec', execf.upper())
            ACCT.ok('PHASE', 'BEGIN', execf)

            # pre phase function

            try:
                cmd = self.get_subcommand(execf.upper())
            except KeyError, exc:
                error.add(exc, options, 'exec', '', sys.exc_info())
                raise E4tSystemError(execf, 'Plugin %s not exists', execf)
            if not cmd:
                LOGGER.error('Phase %s not in command', execf)
                continue
            setattr(options, 'command_obj', self)
            dir(cmd)
            if hasattr(cmd, 'setUp'):
                try:
                    cmd.setUp(options)
                except Exception, exc:
                    error.add(exc, options, 'exec', 'pre',
                              sys.exc_info())
                    raise
            command = cmd()
            try:
                command(options)
                ACCT.ok('EXEC', 'OK', execf)
            except Exception, exc:
                error.add(exc, options, 'job', 'mainloop',
                          sys.exc_info())
                ACCT.ko('EXEC', 'OK', execf)

            # post-phase function

            if hasattr(cmd, 'setUp'):
                try:
                    command.tearDown(options)
                except Exception, exc:
                    error.add(exc, options, 'exec', 'post',
                              sys.exc_info())
            ACCT.ok('PHASE', 'END', execf)

        return error


class SubCommand(type):

    def __init__(cls, name, bases, attrs):
        if hasattr(cls, 'supercommand'):
            cls.supercommand.add_subcommand(name, cls)


class SubCommandProvider(object):

    __metaclass__ = SubCommand

