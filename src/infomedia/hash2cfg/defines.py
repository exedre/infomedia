#!/usr/bin/python
# -*- coding: utf-8 -*-

##
##  This file is part of infomedia framework library
##
##  Copyright (c) 2011-2014 Infomedia Foundation
##
##  Author: Emmanuele Somma (emmanuele_DOT_somma_AT_infomedia_DOT_it)
##
##  Any parts of this program derived from this project,
##  or contributed by third-party developers are copyrighted by their
##  respective authors.
##
##  This program is free software; you can redistribute it and/or modify
##  it under the terms of the GNU General Public License as published by
##  the Free Software Foundation; either version 3 of the License, or
##  (at your option) any later version.
##
##  This program is distributed in the hope that it will be useful,
##  but WITHOUT ANY WARRANTY; without even the implied warranty of
##  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
##  GNU General Public License for more details.
##
##  You should have received a copy of the GNU General Public License
##  along with this program; if not, write to the Free Software
##  Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301, USA
##
##

import re
import logging

from os import getenv, environ
from datetime import datetime
from itertools import imap

from infomedia.vdict import vdict,udict,expansion

LOGGER = logging.getLogger(__name__)

DATE = datetime.now()

# DEFINES

DEFAULT_DEFINES = {
    'THISMONTH': str(DATE.month),
    'THISYEAR': str(DATE.year),
    'TYEAR': str(DATE.year)[2:],
    'PREVYEAR': str(DATE.year - 1),
    'PREVVYEAR': str(DATE.year - 2),
    'PREVVVYEAR': str(DATE.year - 3),
    'NEXTYEAR': str(DATE.year + 1),
    'SRIDATA': getenv('SRIDATA'),
    'YUPD': str(0),
    'UPDATE': str(0),
    'DONT_UPDATE': str(1),
    'NUM': str(0),
    'DATE': DATE.strftime('%Y%m%d'),
    'REVISION_DATE': DATE.strftime('%Y%m%d'),
    'THISDATE': DATE.strftime('%d/%m/%Y'),
    'LONG_DATE': DATE.strftime('%d %B %Y'),
    'DS_DATE': DATE.strftime('%Y-%m-%d'),
    }

## FUNCTIONS IN TABLE DEFINITION
##

MONTHS = [
    '_(Jan)',
    '_(Feb)',
    '_(Mar)',
    '_(Apr)',
    '_(May)',
    '_(Jun)',
    '_(Jul)',
    '_(Aug)',
    '_(Sep)',
    '_(Oct)',
    '_(Nov)',
    '_(Dec)',
    ]

# MACRO_LAST_YEAR Is False if

MACRO_LAST_YEAR = False


def _macro_month(*args):
    """month from 1->Jan to 12->Dec"""

    _ev = int(args[0]) - 1
    return (MONTHS[_ev % 12], 1, 'c')


def _macro_fyear(*args):
    """FYEAR(YEAR,MONTH)
    returns
    \\multicolumn{n}{c}{$THISYEAR}
    or
    empty if
    """

    global MACRO_LAST_YEAR
    assert len(args) == 3

    _month = int(args[1]) - 1
    _year = int(args[0]) - ((1 if _month < 0 else 0))
    _remain = int(args[2])
    _date = datetime.now()
    _yy = _date.year
    _mm = _date.month
    LOGGER.debug(
        '{FYEAR} m/d - remain - M/Y = %d/%d - %d - %d/%d',
        _month,
        _year,
        _remain,
        _mm,
        _yy,
        )
    if _year == _yy:
        MACRO_LAST_YEAR = False
        if _month == 0:

            # emit a multicolumn for remaining months

            if _remain == 1:
                return (str(_year), 1, 'c')
            else:
                return (str(_year), _remain, 'c')
    else:
        if MACRO_LAST_YEAR == False:
            MACRO_LAST_YEAR = True
            return (str(_year), -_month, 'c')

    return None


def __macro_get(name, *args):
    assert args
    assert len(args) > 0

    _nn = 0
    if args and args[0]:
        _mm = re.match(r'^(-?[0-9]+)$', args[0])
        if _mm:
            _nn = int(_mm.group(1))
    _mm = re.match(r'^[A-Z][A-Z0-9_]*$', name)
    if _mm:
        if name in DATASET:
            _ts = DATASET[name]
            return (_ts, _nn)

def _macro_get(name, *args):

    _ret = __macro_get(name, *args)
    if _ret:
        assert len(_ret) == 2
        (_ts, _nn) = _ret
        return  (unicode('%.1f' % _ts[_nn]), 1, 'c')


def _macro_getdate(name, *args):

    _ret = _macro_get(name, *args)
    if _ret:
        assert len(_ret) == 2
        (_ts, _nn) = _ret
        _dd = _ts.dates[_nn]
        return (unicode(_dd), 1, 'c')

MACRO_FNCS = {
    'MONTH': _macro_month,
    'FYEAR': _macro_fyear,
    'GET': _macro_get,
    'GETDATE': _macro_getdate,
    }


NAME = None
SPEC = None
DATASET = None
OPTIONS = None


def _macro_rerepl(matchobj):
    _gg = [_m for _m in matchobj.groups()]
    _ff = _gg.pop(0)
    _res = MACRO_FNCS[_ff](*_gg)
    if _res:
        (_msg, _nn, _cc) = _res
        return '#%s#%s#%s#' % (_msg, _nn, _cc)


def expandfuncs(a_string, _name=None, _spec=None, _dataset=None,
                _options=None, unfold=False):


    # not expand if not basestring

    assert isinstance(a_string, (basestring))
    assert len(a_string) > 0

    _the_string = unicode(a_string)

    # save globals

    global NAME, SPEC, DATASET, OPTIONS
    if _name:
        NAME = _name
    if _spec:
        SPEC = _spec
    if _dataset:
        DATASET = _dataset
    if _options:
        OPTIONS = _options


    # no expansion needed

    if '%' not in _the_string:
        return _the_string

    # else

    a_macro_name_regexp = r'%([A-Z]+)\(([^),]+)(,[^,)])*\)'
    a_macro_call_regexp = r'%([A-Z]+)\(([^,)]+)(?:,([^,)]+))?(?:,([^,)]+))?\)'

    _match = re.search(a_macro_name_regexp, _the_string, re.I)
    if _match:
        _the_string_before = _the_string
        while '%' in _the_string:
            _the_string = re.sub(a_macro_call_regexp, 
                                 _macro_rerepl, 
                                 _the_string, 
                                 re.I)
            if _the_string is None or _the_string == _the_string_before:
                break
            _the_string_before = _the_string
    if _the_string:
        if unfold:
            _unfolding_regexp = r'#([^#]+)#[^#]+#[^#]+#'
            _the_string = re.sub(_unfolding_regexp, '\\1',
                          _the_string,
                          re.I)
        return _the_string

def expandlistfunc(a_string, _name=None, _spec=None, _dataset=None,
                   _options=None, unfold=False):
    _ret = imap(lambda x: expandfuncs(x, _name, _spec, _dataset, \
                                 _options, unfold), a_string)
    return _ret


class DefineSet(udict):
    """
    DefineSet is
    """

    def __init__(self, indict=None):
        """
        :param indict: input dictionary
        :type indict: dict
        :default indict: None
        """
        udict.__init__(self)

        self.setup()

        # If has input dictionary reads in
        if indict:
            self.update(indict)

    def setup(self):
        """

        """

        # Insert Environment variables into DefineSet

        self.update(environ)

        # Insert DEFAULT_DEFINES

        self.update(DEFAULT_DEFINES)

        self._varprog = None

    def report(self):
        _rep = ''
        for (k, j) in self.items():
            _rep += '%s=%s\n' % (k, j)
        return _rep

    def update(self, indict):
        """

        """
        _hd = udict(indict)
        vdict.update(self, _hd)
        if hasattr(indict, '_vaprog'):
            self._varprog = indict._vaprog
        else:
            self._varprog = re.compile(r'\$(\w+|\{[^}]*\})')

    def update_from_strings(self, definitions):
        """

        """
        if definitions:
            _dd = udict([j.split('=') for j in definitions])
            self.update(_dd)
            if 'DATE' in _dd:
                _dt = _dd['DATE'].replace('/', '-')
                _re1 = r'[0-9][0-9][0-9][0-9]-[0-9][0-9][0-9]?-[0-9][0-9]?'
                _re2 = r'[0-9][0-9][0-9]?-[0-9][0-9]?-[0-9][0-9][0-9][0-9]'
                if re.match(_re1, _dt):
                    _fmt = '%Y-%m-%d'
                elif re.match(_re2, _dt):
                    _fmt = '%d-%m-%Y'
                else:
                    LOGGER.error('DATE format non recognized in %s',
                                 _dd['DATE'])
                    return (ValueError,
                            'DATE format non recognized in %s' % _dd['DATE'])

                # re-definition of base defines on actual date

                _date_t = datetime.strptime(_dt, _fmt)
                _defines = {
                    'THISMONTH': str(_date_t.month),
                    'THISYEAR': str(_date_t.year),
                    'TYEAR': str(_date_t.year)[2:],
                    'PREVYEAR': str(_date_t.year - 1),
                    'PREVVYEAR': str(_date_t.year - 2),
                    'PREVVVYEAR': str(_date_t.year - 3),
                    'NEXTYEAR': str(_date_t.year + 1),
                    'DATE': _date_t.strftime('%Y%m%d'),
                    'THISDATE': _date_t.strftime('%d/%m/%Y'),
                    'LONG_DATE': _date_t.strftime('%d %B %Y'),
                    'DS_DATE': _date_t.strftime('%Y-%m-%d'),
                    }
                self.update(_defines)

                if DATE > _date_t:
                    LOGGER.warn('This is a REVISION for date %s',
                                self['LONG_DATE'])
                elif DATE < _date_t:
                    LOGGER.warn('This is a *P*REVISION for date %s',
                                self['LONG_DATE'])

    def expand(self, a_string):
        """Expand parameters in form of $var and ${var}.  Unknown variables
        are left unchanged.

        :param a_string: string to expand
        :type a_string: unicode

        :returns: the string with parameters expanded
        """

        if len(self) == 0:
            self.setup()

        if isinstance(a_string, udict):
            _ret = udict()
            for (k, j) in a_string.items():
                _ret[k] = self.expand(unicode(j))
            return _ret

        if '$' not in a_string:
            return a_string

        if not hasattr(self, '_varprog') or not self._varprog:
            self._varprog = re.compile(r'\$(\w+|\{[^}]*\})')

        i = 0
        while True:
            _match = self._varprog.search(a_string, i)
            if not _match:
                break
            (i, j) = _match.span(0)
            _name = _match.group(1)
            if _name.startswith('{') and _name.endswith('}'):
                _name = _name[1:-1]
            if _name in self:
                _tail = a_string[j:]
                a_string = a_string[:i] + unicode(self[_name])
                i = len(a_string)
                a_string += _tail
            else:
                i = j
        return a_string


DEFINE_SET = DefineSet()


def full_expand(a_string, define_set):
    """
    Expand contents of aString
    :param aString: the string to expanf
    :paran define_set: dictionary of expansion

    :Example:

    Use::
        expanded = full_expand(string_to_expand,define_set)

    """

    return expansion(a_string, define_set)

__all__ = """
expandfuncs
expandlistfunc
expansion
full_expand
DATE
DEFAULT_DEFINES
DEFINE_SET
DefineSet
""".split()
